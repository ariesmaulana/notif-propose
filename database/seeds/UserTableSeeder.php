<?php

use Illuminate\Database\Seeder;
use App\Model\Role;
use App\Model\Permission;
use App\Model\Profile;
use App\Model\User;
use Webpatser\Uuid\Uuid;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // seed permission
        Permission::insert([	
            ['id'=>Uuid::generate()->string,'name'=>'view user','slug'=>'view-user'], 
            ['id'=>Uuid::generate()->string,'name'=>'create user','slug'=>'create-user'], 
            ['id'=>Uuid::generate()->string,'name'=>'update user','slug'=>'update-user'],
            ['id'=>Uuid::generate()->string,'name'=>'delete user','slug'=>'delete-user'],
            ['id'=>Uuid::generate()->string,'name'=>'access config','slug'=>'access-config'],
            ['id'=>Uuid::generate()->string,'name'=>'global config','slug'=>'global-config'],
            ['id'=>Uuid::generate()->string,'name'=>'view access','slug'=>'view-access'], 
            ['id'=>Uuid::generate()->string,'name'=>'create access','slug'=>'create-access'], 
            ['id'=>Uuid::generate()->string,'name'=>'update access','slug'=>'update-access'],
            ['id'=>Uuid::generate()->string,'name'=>'delete access','slug'=>'delete-access'],
            ['id'=>Uuid::generate()->string,'name'=>'view role','slug'=>'view-role'],
            ['id'=>Uuid::generate()->string,'name'=>'create role','slug'=>'create-role'],
            ['id'=>Uuid::generate()->string,'name'=>'update role','slug'=>'update-role'],
            ['id'=>Uuid::generate()->string,'name'=>'delete role','slug'=>'delete-role'],
        ]);
        $admin = [
            'view-user',
            'create-user',
            'update-user',
            'delete-user',
            'view-access',
            'create-access',
            'update-access',
            'delete-access',
            'view-role',
            'create-role',
            'update-role',
            'delete-role',
            'access-config',
            'global-config'
        ];
        $moderator = [
            'view-user',
            'create-user',
            'update-user'
        ];

        // // seed roles
        Role::insert([
            ['id'=>Uuid::generate()->string,'name'=>'Admin','slug'=>'admin','permissions'=>json_encode($admin)],
            ['id'=>Uuid::generate()->string,'name'=>'Moderator','slug'=>'moderator','permissions'=>json_encode($moderator)]
        ]);
        
        User::insert([
            ['id'=>'9a7e14d0-408d-11e9-b3cc-d91603d90e55','name'=>'admin','email'=>'admin@example.com','password'=>bcrypt('password123'),'roles'=>json_encode(['admin'])]
        ]);
        User::find('9a7e14d0-408d-11e9-b3cc-d91603d90e55')->profile()->save(new Profile(['status'=>'active']));

        User::create(
            ['name'=>'moderator','email'=>'moderator@example.com','password'=>bcrypt('password123'),'roles'=>json_encode(['moderator'])]
        )->profile()->save(new Profile());

    }
}
