@extends('layouts.dashboard')
@section('content')
<div class="row">
    <div class="col-lg-12" id="app">
        <config-global
            app_name="{{ str_replace('-',' ',$list['name']) }}"
            limit_page="{{ $list['limit'] }}"
        />
    </div>
</div>
@endsection