@extends('layouts.reg')

@section('content')
<div class="p-5">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">Selamat Datang</h1>
    </div>
    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="form-group">
        <span class="label label-warning">
            <strong> {{ session('message') }}</strong>
        </span>
        <input id="email"  type="email" class="form-control form-control-user {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Enter Email...">     
        </div>
        <div class="form-group">
        <input id="password" type="password" class="form-control form-control-user{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Enter Password...">

        </div>
        <div class="form-group">
        {{-- <div class="custom-control custom-checkbox small">
            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

            <label class="form-controll-label" for="remember">
                {{ __('Remember Me') }}
            </label>
        </div> --}}
        </div>
        <button class="btn btn-primary btn-user btn-block">
        Login
        </button>
    </form>
    <hr>
    <div class="text-center">
        @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        @endif
    </div>
    <div class="text-center">
        {{-- @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('register') }}">{{ __('Register') }}</a>
        @endif --}}
    </div>
</div>
@endsection
