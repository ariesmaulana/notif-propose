<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('dashboard') }}">
      <div class="sidebar-brand-icon rotate-n-15">
      </div>
    <div class="sidebar-brand-text mx-3">{{ str_replace('-',' ', config('app.name') ) }}</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ config('webapp.activeRoot') === 'dashboard' ? "active" : "" }}">
      <a class="nav-link" href="{{ route('dashboard') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
      </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
      User
    </div>
    <li class="nav-item {{ config('webapp.activeRoot') === 'user' ? "active" : "" }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#slideUser" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-user"></i>
            <span>User</span>
        </a>
        <div id="slideUser" class="collapse {{ config('webapp.activeRoot') === 'user' ? "show" : "" }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item {{ config('webapp.activeChild') === 'user' ? "active" : "" }}" href="{{ route('list-user') }}">Daftar</a>
                <a class="collapse-item {{ config('webapp.activeChild') === 'user-add' ? "active" : "" }}" href="{{ route('add-user') }}">Tambah</a>
            </div>
        </div>
    </li>

    {{-- <div class="sidebar-heading">
      Pengaturan 
    </div> --}}
    {{-- <li class="nav-item {{ config('webapp.activeRoot') === 'config' ? "active" : "" }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#slideConfig" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Pengaturan</span>
        </a>
        <div id="slideConfig" class="collapse {{ config('webapp.activeRoot') === 'config' ? "show" : "" }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item {{ config('webapp.activeChild') === 'global' ? "active" : "" }}"  href="{{ route('global-config') }}">Global</a>
               <a class="collapse-item" href="{{ route('role') }}">Peran</a>
                <a class="collapse-item" href="cards.html">Akses</a>
            </div>
        </div>
    </li> --}}

   

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>