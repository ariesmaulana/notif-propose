@extends('layouts.dashboard')
@section('content')
<div id="app">
    <user-edit
        user_id='{{ $id }}'
        as_profile=false
    />
</div>
@endsection