@extends('layouts.dashboard')
@section('content')
<div id='app'>
    <h1 class="h3 mb-1 text-gray-800">Daftar Pengguna</h1>
   
    <user-list
        permission="{{ json_encode($userPermission) }}"
    />
</div>
@endsection