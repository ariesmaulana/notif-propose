@extends('layouts.dashboard')
@section('content')
<div id="app">
    <user-edit
        user_id='{{ auth::user()->id }}'
        as_profile=true
    />
</div>
@endsection