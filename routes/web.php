<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function () {
    Route::get('', 'dashboard\HomeController@index')->name('dashboard');

    Route::group(['prefix' => 'user'], function () {
        Route::get('', 'dashboard\UserController@index')->name('list-user');
        Route::post('', 'dashboard\UserController@store')->name('post-user');
        Route::delete('', 'dashboard\UserController@destroy')->name('delete-user');
        Route::get('/show/{id}', 'dashboard\UserController@show')->name('show-user');
        Route::post('/detail', 'dashboard\UserController@detail')->name('detail-user');
        Route::put('', 'dashboard\UserController@update')->name('update-user');
        Route::post('/list', 'dashboard\UserController@lists')->name('list-user-vue');
        Route::get('add', 'dashboard\UserController@add')->name('add-user');
        Route::get('profile', 'dashboard\UserController@profile')->name('profile');
    });

    Route::group(['prefix' => 'config'], function () {
        Route::get('', 'dashboard\ConfigController@index')->name('global-config');    
        Route::post('/save', 'dashboard\ConfigController@save')->name('save-config');    
        Route::get('/role', 'dashboard\ConfigController@role')->name('role');    
        Route::get('/get-role', 'dashboard\ConfigController@getRole')->name('get-role');    
    });
});

Route::get('mail/','ContentController@bulk');
Route::get('mail/{id}','ContentController@detailEmail');