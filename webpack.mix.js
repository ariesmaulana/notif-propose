const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
   resolve: {
       alias: {
           "@": path.resolve(
               __dirname,
               "resources/js"
           )
       }
   }
});

// build asset css
mix.styles([
   'resources/dashboard/css/sb-admin-2.min.css',
   'resources/dashboard/css/custom.css',
], 'public/css/dashboard/dashboard.css');


mix.scripts([
   'resources/dashboard/js/jquery/jquery.min.js',
   'resources/dashboard/js/jquery-easing/jquery.easing.min.js',
   'resources/dashboard/js/sb-admin-2.min.js',
], 'public/js/dashboard/dashboard.js');


mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');
