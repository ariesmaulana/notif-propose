<?php

return [
    'menu'=>['dashboard','user','user-add'],
    'activeRoot'=>'',
    'activeChild'=>'',
    'limitPage'=>env('APP_LIMIT', 10)
];
