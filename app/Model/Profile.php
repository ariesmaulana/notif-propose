<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\MyUuid;

class Profile extends Model
{
    use MyUuid;
    protected $guarded = ['id'];
    public $incrementing = false;
    public function user(){
        return $this->belongsTo('App\Model\User','user_id');
    }

}
