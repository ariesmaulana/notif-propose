<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\MyUuid;

class Permission extends Model
{
    use MyUuid;
    protected $guarded = ['id'];
    public $incrementing = false;
}
