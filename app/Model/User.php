<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\MyUuid;

class User extends Authenticatable
{
    use Notifiable, MyUuid;
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile() {
        return $this->hasOne('App\Model\Profile','user_id');
    }

    public function getUserRoleAttribute() {
        if(!empty($this->roles)){
            return json_decode($this->roles);
        }
        return [];
    }

    public function getPermissionListAttribute() {
        $roles = json_decode($this->roles);
        $permissions = Role::whereIn('slug',$roles)
                        ->get()
                        ->map(function($item) {
                            return json_decode($item['permissions']);
                        })
                        ->flatten()
                        ->toArray();
        return $permissions;
    }

}
