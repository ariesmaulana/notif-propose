<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\ArrayMustNotEmpty;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'email'=>'required|email|unique:users,email',
                    'password'=>'required|confirmed|min:5',
                    'name'=>'required|min:5',
                    'image'=>'required|mimes:jpeg,png,jpg',
                    'roles'=> 'required'
                ];
            }
            case 'PUT':
            {
                return [
                    'password'=>'nullable|confirmed|min:5',
                    'name'=>'required:min:5',
                    'image'=>'nullable|mimes:jpeg,png,jpg',
                    'roles'=> 'required'
                ];
            }
            default:break;
        }
    }

     public function messages()
    {
        return [
            'name.required'=>'Harap Isi Field Nama',
            'name.min'=>'Nama minimal 5 karakter',
            'email.required'=>'Harap lengkapi email',
            'email.unique'=>'Email sudah digunakan',
            'email.email'=>'Harap gunakan email yang benar',
            'password.required'=>'Harap isi password',
            'password.min'=>'Minimal password 5 karakter',
            'password.confirmed'=>'Password tidak sesuai',
            'image.required'=>'Harap lengkapi gambar',
            'sex.required'=>'Harap pilih jenis kelamin',
            'image.required'=>'Lengkapi foto Anda',
            'image.mimes'=>'Pastikan gambar merupakan jpeg, jpg, atau png',
            'roles.required'=>'Role wajib diisi'
        ];
    }

}
