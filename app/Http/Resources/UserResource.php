<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id"=>$this->id,
            "name"=>$this->name,
            "username"=>$this->name,
            "email"=>$this->email,
            "sex"=>$this->profile->sex,
            "status"=> $this->profile->status === 'active' ? true : false,
            "image"=> url('/images/avatar/'.$this->profile->image),
            "roles"=>$this->user_role,
            "permissions"=>$this->permission_list
        ];
    }
}
