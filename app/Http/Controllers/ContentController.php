<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;
use App\Jobs\SendNotifMail;
use DB;
class ContentController extends Controller
{

    public function bulk() {

        $cek = DB::table('jobs')->first();
        $payload= json_decode($cek->payload);
        $data = $payload->data;
        $model = unserialize($data->command);
        dd($model->response->id);
        return response()->json($data);


        $list = Content::where('status','queue')->orderBy('created_at','desc')->limit(5)->get();
       
        foreach($list as $row) {
            $this->detailEmail($row->id);
        }
    }
    public function detailEmail($id) {
        $content = Content::find($id);
        // $email = new NotifMail($content);
        // Mail::to($content->email)->send(new NotifMail($content));
       $data = SendNotifMail::dispatch($content);
        // return view('content.mail',compact('content'));
    }
}
