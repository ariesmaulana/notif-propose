<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    function __construct() {
        config(['webapp.activeRoot' => 'dashboard']);
    }

    function index() {
        return view('dashboard.index');
    }
}
