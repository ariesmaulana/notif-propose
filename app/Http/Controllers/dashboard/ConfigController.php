<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Role;
class ConfigController extends Controller
{
    function __construct() {
        config(['webapp.activeRoot' => 'config']);
    }


    public function role() {
        return view('config.role');
    }
    public function getRole() {
        $role =  Role::all()->pluck('name','slug');
        return $role;
    }
    function index() {
        config(['webapp.activeChild' => 'global']);
        $list = [
            'name'=>config('app.name'),
            'limit'=>config('webapp.limitPage')
        ];
        return view('config.global', compact('list'));
    }

    function save(Request $request) {
        try {
            $env_update = $this->changeEnv([
                'APP_LIMIT'   => $request->limit_page,
                'APP_NAME'   => str_replace(" ","-",$request->web_name),
            ]);
            \Artisan::call('config:cache');
            // return redirect()->back();
             return response()->json(['status'=>'success'], 200);
        } catch (\Throwable $th) {
            abort('500');
        }
    }

    protected function changeEnv($data = array()){
        if(count($data) > 0){

            // Read .env-file
            $env = file_get_contents(base_path() . '/.env');

            // Split string on every " " and write into array
            $env = preg_split('/\s+/', $env);;

            // Loop through given data
            foreach((array)$data as $key => $value){

                // Loop through .env-data
                foreach($env as $env_key => $env_value){

                    // Turn the value into an array and stop after the first split
                    // So it's not possible to split e.g. the App-Key by accident
                    $entry = explode("=", $env_value, 2);

                    // Check, if new key fits the actual .env-key
                    if($entry[0] == $key){
                        // If yes, overwrite it with the new one
                        $env[$env_key] = $key . "=" . $value;
                    } else {
                        // If not, keep the old one
                        $env[$env_key] = $env_value;
                    }
                }
            }

            // Turn the array back to an String
            $env = implode("\n", $env);

            // And overwrite the .env with the new data
            file_put_contents(base_path() . '/.env', $env);
            
            return true;
        } else {
            return false;
        }
    }
}
