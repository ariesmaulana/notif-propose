<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Profile;
use DB;
use Auth;
use App\Http\Resources\UserResource;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    public $limit  = '';
    function __construct() {
        config(['webapp.activeRoot' => 'user']);
        $this->limit = config('webapp.limitPage');
    }

    function index() {
        config(['webapp.activeChild' => 'user']);
        $userPermission = Auth::user()->permission_list;
        return view('user.index', compact('userPermission'));
    }

    function lists(Request $request) {
        $payload = $request->all();
        if($payload) {
            $user = User::with('profile')
                    ->where('email', 'like', '%' . $payload['email'] . '%')
                    ->where('name', 'like', '%' . $payload['name'] . '%')
                    ->where('id','!=','9a7e14d0-408d-11e9-b3cc-d91603d90e55')
                    ->paginate($this->limit);
        }else{
            $user = User::with('profile')
            ->where('id','!=','9a7e14d0-408d-11e9-b3cc-d91603d90e55')->paginate($this->limit);
        }
        return UserResource::collection($user);
    }
    
    function add() {
        config(['webapp.activeChild' => 'user-add']);
        return view('user.add');
    }

    function store(UserRequest $request) {
        
        $response =[
            'success'=>null
        ];
        $code = 200;
        $roles = explode(',',$request->roles);
        $data['user'] = [
            'name'=>$request->name,
            'password'=>bcrypt($request->password),
            'email'=>$request->email,
            'roles'=>json_encode($roles)
        ];
        $data['profile'] = [
            'name'=>$request->name,
            'status'=>$request->status
        ];
        if ($request->hasFile('image')) {
            $filename = strtotime("now").'.'.$request->image->extension();
            $data["profile"]["image"] = $filename;
            $request->image->storeAs("avatar", $filename,'upload'); 
        }
       
        DB::beginTransaction();
        try {
            User::create($data['user'])
                ->profile()->save(new Profile($data['profile']));
        DB::commit();
        $response['success'] = true;
        } catch (\Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();
            $code = 500;
        }
        return response()->json($response, $code);
    }

    function destroy(Request $request) {
        $response =[
            'success'=>null
        ];
        $code = 200;
      
        DB::beginTransaction();
        try {
            $user = User::find($request->id);
            $user->delete();
            DB::commit();
            $response['success'] = true;
        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();
            $response['success'] = false;
            $response['message'] = $e->getMessage();
            $code = 500;
        }
        return response()->json($response, $code);
    }
    
    public function show($id) {
        
        return view('user.edit',compact('id'));
    }

    public function detail(Request $request) {
        $detail = User::with('profile')->find($request->id);
        return new UserResource($detail);
    }

    public function profile() {
        return view('user.profile');
    }

    public function update(UserRequest $request) {
        $response =[
            'success'=>null
        ];
        $code = 200;       
        $roles = explode(',',$request->roles);
        $data['user'] = [
            'name'=>$request->name,
            'roles'=>json_encode($roles)
        ];
        if($request->password) {
            $data['user']['password'] = bcrypt($request->password);
        }
        $data['profile'] = [
            'name'=>$request->name,
            'status'=>$request->status
        ];
        if ($request->hasFile('image')) {
            $filename = strtotime("now").'.'.$request->image->extension();
            $data["profile"]["image"] = $filename;
            $request->image->storeAs("avatar", $filename,'upload'); 
        }
       
        DB::beginTransaction();
        try {
            User::find($request->id)->update($data['user']);
            Profile::where('user_id',$request->id)->update($data['profile']);
        DB::commit();
        $response['success'] = true;
        } catch (\Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();
            $code = 500;
        }
        return response()->json($response, $code);
    }
}
